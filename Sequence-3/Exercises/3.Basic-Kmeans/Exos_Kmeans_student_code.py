# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 07:46:10 2020

@author: capliera
"""

# used for manipulating directory paths
import os

# Scientific and vector computation for python
import numpy as np

# Used to load MATLAB mat datafile format
from scipy.io import loadmat

# library written for this exercise
import utils

from matplotlib import pyplot



def findClosestCentroids(X, centroids):
    """
    Computes the centroid memberships for every example.
    
    Parameters
    ----------
    X : array_like
        The dataset of size (m, n) where each row is a single example. 
        That is, we have m examples each of n dimensions.
        
    centroids : array_like
        The k-means centroids of size (K, n). K is the number
        of clusters, and n is the data dimension.
    
    Returns
    -------
    idx : array_like
        A vector of size (m, ) which holds the centroids assignment for each
        example (row) in the dataset X.
    """
    
    # Set K
    #K = centroids.shape[0]

    idx = np.zeros(X.shape[0], dtype=int)

    # ====================== Closest centroid computation ======================

    for i in np.arange(idx.size):
        
        J = np.sqrt(np.sum(np.square(X[i] - centroids), axis = 1))
            
        idx[i] = np.argmin(J)
    
    # =============================================================
    return idx

def computeCentroids(X, idx, K):
    """
    Returns the new centroids by computing the means of the data points
    assigned to each centroid.
    
    Parameters
    ----------
    X : array_like
        The datset where each row is a single data point. That is, it 
        is a matrix of size (m, n) where there are m datapoints each
        having n dimensions. 
    
    idx : array_like 
        A vector (size m) of centroid assignments (i.e. each entry in range [0 ... K-1])
        for each example.
    
    K : int
        Number of clusters
    
    Returns
    -------
    centroids : array_like
        A matrix of size (K, n) where each row is the mean of the data 
        points assigned to it.
    
    How it is working
    ------------
    For every centroid, compute mean of all points that
    belong to it. Concretely, the row vector centroids[i, :]
    contain the mean of the data points assigned to
    cluster i.

    """
    # Useful variables
    m, n = X.shape
    centroids = np.zeros((K, n))

    # ====================== Centroid computation ======================

    for i in np.arange(K):
        centroids[i] = np.mean(X[idx == i], axis = 0)    
    
    # =============================================================
    return centroids

def kMeansInitCentroids(X, K):
    """
    This function initializes K centroids that are to be used in K-means on the dataset x.
    
    Parameters
    ----------
    X : array_like 
        The dataset of size (m x n).
    
    K : int
        The number of clusters.
    
    Returns
    -------
    centroids : array_like
        Centroids of the clusters. This is a matrix of size (K x n).
    
    """
    m, n = X.shape
    
    centroids = np.zeros((K, n))

    # ====================== Random centroid intialization ======================

    randidx = np.random.permutation(X.shape[0])
    # Take the first K examples as centroids
    centroids = X[randidx[:K], :]
    
    # =============================================================
    return centroids

# DATA SET LOADING
data = loadmat(os.path.join('Data', 'ex7data2.mat'))
X = data['X']
pyplot.scatter(X[:, 0], X[:, 1], s=50);
pyplot.title('Data training set')

## RUNNING THE FIRST ITERATION OF KMEANS ALGO STEP BY STEP

# Before starting Kmeans clustering: Set the centroid number K and 
# select an initial set of centroids

K = 3   # 3 Centroids => the label of each sample might be 0, 1, ... K-1
initial_centroids = np.array([[3, 3], [6, 2], [8, 5]]) # 3 samples of the dataset 
                                                       #are selected as initial centroids
print("Initial centroids :\n ", initial_centroids)

# Kmean algo first step: Assign each training sample to its closest centroid
idx = findClosestCentroids(X, initial_centroids)
print('Closest centroids for the first 3 training examples:')
print(idx[:3])
print("\n")

# Kmean algo second step: Recompute the mean of each centroid using the  
# points assigned to it.
centroids = computeCentroids(X, idx, K)
print('Centroids computed after the assignment step:')
print(centroids)
print("\n")

## RUNNING A KMEANS ALGORITHM WITH DIFFERENT CENTROIDS INITIALIZATIONS
# That's mean several iterations of the two basic previous steps

# Settings for running K-Means
K = 3
max_iters = 10

# Centroids initialization: either manually tuned or randomly selected 

initial_centroids = np.array([[3, 3], [6, 2], [8, 5]])

print("Home made Kmeans algo\n")
print("Initial centroids :\n ", initial_centroids)

# Run K-Means algorithm. The 'true' at the end tells the function to plot
# the progress of K-Means
centroids, idx, anim = utils.runkMeans(X, initial_centroids,
                                       findClosestCentroids, computeCentroids, max_iters, True)
anim
print("Final centroids :\n", centroids)
print("\n")

# USING THE SKLEARN KMeans function

# TO BE COMPLETED



