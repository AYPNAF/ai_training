# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 15:31:10 2020

@author: capliera
"""

# used for manipulating directory paths
import os

# Scientific and vector computation for python
import numpy as np

# Plotting library
from matplotlib import pyplot

# will be used to load MATLAB mat datafile format
from scipy.io import loadmat

# library written for this exercise providing additional functions
import utils
  

# EXAMPLE DATASET 1: linear SVM
#==============================
# Load from ex6data1
# You will have X, y as keys in the dict data
data = loadmat(os.path.join('Data', 'ex6data1.mat'))
X, y = data['X'], data['y'][:, 0]

# Plot training data
utils.plotData(X, y)

# Linear SVM parameter
# You should try to change the C value below and see how the decision
# boundary varies (e.g., try C = 1000)
C = 100

model = utils.svmTrain(X, y, C, utils.linearKernel, 1e-3, 20)
utils.visualizeBoundaryLinear(X, y, model)
valeur="C="+str(C)
pyplot.title(valeur)

# EXAMPLE DATASET 2: Gaussian SVM
# Load from ex6data2
# You will have X, y as keys in the dict data
data = loadmat(os.path.join('Data', 'ex6data2.mat'))
X, y = data['X'], data['y'][:, 0]

# Plot training data
pyplot.figure()
utils.plotData(X, y)

# Gaussian Kernel SVM Parameters
C = 1
sigma = 0.1

model= utils.svmTrain(X, y, C, utils.gaussianKernel, args=(sigma,))
pyplot.figure()
utils.visualizeBoundary(X, y, model)
valeur="(C,sigma)="+str(C)+", "+str(sigma)
pyplot.title(valeur)

# EXAMPLE DATATSET 3: looking for optimal parameter for Gaussian SVM
# Load from ex6data3
# You will have X, y, Xval, yval as keys in the dict data
data = loadmat(os.path.join('Data', 'ex6data3.mat'))
X, y, Xval, yval = data['X'], data['y'][:, 0], data['Xval'], data['yval'][:, 0]

# Plot training data
pyplot.figure()
utils.plotData(X, y)

#SVM classification TO BE COMPLETED
