#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 15:35:29 2020

@author: arya.yazdan-panah
"""

import numpy as np
from os.path import join
import matplotlib.pyplot as plt
import csv
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler, MinMaxScaler


plt.close('all')

# DATA LOADING: X is the feature matrix and Y is the label vector

filename = 'diabetes.csv'
dirpath = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/AI/deep/Sequence-1/Datasets'
filepath = join(dirpath, filename)
print(filepath)

file = open(filepath, "r")
data = csv.reader(file, delimiter = ",")
data = np.array(list(data))

X = data[:, 0:8]
Y = data[:, 8]

# GAUSSIAN NAIVE BAEYSIAN CLASSIFIER
# Data conversion towards integer values
data = data.astype(float)
X = data[:, 0:8]
Y = data[:, 8]

# Data splitting: 70% for the training set and 30% for the testing set

line = len(X)
column = len(X[0])
precent = 7/10
X_train = X[0:int(line*precent), :]
Y_train = Y[0:int(line*precent)]

X_test = X[int(line*precent):line-1, :]
Y_test = Y[int(line*precent):line-1]

# Naive bayes classification

classifier = GaussianNB()
classifier.fit(X_train, Y_train)

Y_pred_NB = classifier.predict(X_test)

# Training and testing Accuracies: computing the % of good answers
Y_diff_NB = sum(abs(np.subtract(Y_test, Y_pred_NB)))/len(Y_test)

# Making the Confusion Matrix
conf_mat_NB = confusion_matrix(Y_test, Y_pred_NB)

# K-NN classification

neigh = KNeighborsClassifier(n_neighbors=5)
neigh.fit(X_train, Y_train)
Y_pred_KNN = neigh.predict(X_test)
# Accuracy score

Y_diff_KNN = sum(abs(np.subtract(Y_test, Y_pred_KNN)))/len(Y_test)

conf_mat_KNN = confusion_matrix(Y_test, Y_pred_KNN)

# Standard-scaler

scaler = StandardScaler()
X_norm = scaler.fit_transform(X)

X_train = X_norm[0:int(line*precent), :]
X_test = X_norm[int(line*precent):line-1, :]

neigh_norm = KNeighborsClassifier(n_neighbors=5)
neigh_norm.fit(X_train, Y_train)
Y_pred_KNN_norm = neigh_norm.predict(X_test)

Y_diff_KNN_norm_1 = sum(abs(np.subtract(Y_test, Y_pred_KNN_norm)))/len(Y_test)
conf_mat_KNN_norm = confusion_matrix(Y_test, Y_pred_KNN_norm)

# min-max

scaler = scaler = MinMaxScaler()
X_norm = scaler.fit_transform(X)

X_train = X_norm[0:int(line*precent), :]
X_test = X_norm[int(line*precent):line-1, :]

neigh_norm = KNeighborsClassifier(n_neighbors=3)
neigh_norm.fit(X_train, Y_train)
Y_pred_KNN_norm = neigh_norm.predict(X_test)

Y_diff_KNN_norm_2 = sum(abs(np.subtract(Y_test, Y_pred_KNN_norm)))/len(Y_test)
conf_mat_KNN_norm = confusion_matrix(Y_test, Y_pred_KNN_norm)
