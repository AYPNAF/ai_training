# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 10:50:26 2020

@author: capliera
"""

import numpy as np
from os.path import join
import matplotlib.pyplot as plt
import csv

plt.close('all')

# DATA LOADING: X is the feature matrix and Y is the label vector

filename = r'caesariandata.csv'
dirpath = r'Student_Dataset'
filepath = join(dirpath, filename)
print(filepath)

file = open(filepath,"r")
data = csv.reader(file, delimiter = ",")
data = np.array(list(data))
data = data.astype(int)
X = data[:, 0:5]
Y = data[: , 5]

# DATA REPARTITION

#TO BE COMPLETED


# Manual Trainind and Testing sets definition (70% and 30% of the total data set)
# for the first trial and then by using with the train_test_split function for 
# the second trial

# TO BE COMPLETED

# Autatic data splitting for the second trial

# Naive bayes classification

# TO BE COMPLETED

# Accuracies and confusion matrix

# TO BE COMPLETED





