# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 15:15:17 2020

@author: capliera
"""

import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from os.path import join
import matplotlib.pyplot as plt
import csv

plt.close('all')

# DATA LOADING: X is the feature matrix and Y is the label vector

filename = r'creditcard.csv'
dirpath = r'Student_Dataset'
filepath = join(dirpath, filename)
print(filepath)

file = open(filepath,"r")
data = csv.reader(file, delimiter = ",")
data = np.array(list(data))

X = data[:, 0:30]
Y = data[: , 30]


# DATA SET REPARTITION

# TO BE COMPLETED

# GAUSSIAN NAIVE BAEYSIAN CLASSIFIER

# TO BE COMPLETED

# K-NN Classification

# TO BE COMPLETED

